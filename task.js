const fs = require('fs'); // either node_modules or inbuilt module lai path dina parena


// task
function write(name, content) {
    return new Promise(function (resolve, reject) {
        fs.writeFile(name, content, function (err, done) {
            if (err) {
                reject(err);
            } else {
                resolve(done)
            }
        });
        console.log('non blocking');
    })

}

fs.readFile('kishor.txt', 'UTF-8', function (err, done) {
    if (err) {
        console.log('error reading file >>', err);
    } else {
        console.log('success >>', done);
    }
})
fs.rename('test.txt', 'newtest.txt', function (err, done) {
    if (err) {
        console.log('error reading file >>', err);
    } else {
        console.log('success >>', done);
    }
})
fs.unlink('a', function (err, done) {
    if (err) {
        console.log('error reading file >>', err);
    } else {
        console.log('success >>', done);
    }
})
//result should be handled in execution
// write('kishor.txt', 'weclome')
//     .then(function (data) {
//         console.log('data >>', data);
//     })
//     .catch(function (err) {
//         console.log('err', err);
//     })

module.exports = {
    write
}
