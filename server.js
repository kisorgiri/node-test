const express = require('express');

const app = express(); // app will be entire express framework
const PORT = 9090;


app.get('/blog', function (req, res) {
    console.log('at get request');
    res.json({
        msg: 'hello from express'
    });
})
app.get('/home', function (req, res) {
    console.log('at get request');
    res.json({
        msg: 'hello from home'
    });
})
app.get('/write/:name/:content', function (req, res, next) {
    // this is not a routing level middleware 
    // this is also applicaiton level middleware
    console.log('applciation level middleware for path');
    // next();
    res.json({
        params: req.params,
        query: req.query
    });
});

app.put('/read', function (req, res, next) {
    // this is not a routing level middleware 
    // this is also applicaiton level middleware
    console.log('applciation level middleware for path');
    next();
});
app.delete('/abc', function (req, res, next) {
    // this is not a routing level middleware 
    // this is also applicaiton level middleware
    console.log('applciation level middleware for path');
    next();
});

app.use(function (req, res, next) {
    // application level middleware
    // aafaile req res object sanga kaam garincha vane application level middleware ho

    // res.sendStatus(200);
    // res.status(200);
    // res.json({
    //     msg: 'you are blocked from middleware'
    // })
    // res.status(200).json({
    //     msg: 'you are blocked from middleware'
    // })
    req.kishor = 'i am kisor';
    next();

});
app.use(function (req, res, next) {
    req.kishor = 'lf'
    console.log('i am at 2nd application level middleware');
    // res.send('hi from second middleware');
    next();
})
app.get('/abc', function (req, res, next) {
    console.log('at get request');
    res.json({
        msg: 'hello from about'
    });
})

app.get('/contact', function (req, res) {
    console.log('req.value >>', req.kishor);
    res.json({
        msg: 'from post'
    })
})
app.put('/login', function (req, res) {
    res.json({
        msg: 'from put'
    })
})
app.delete('/login', function (req, res) {
    res.json({
        msg: 'from delete'
    })
})



app.listen(PORT, function (err, done) {
    if (!err) {
        console.log(`server listening at port ${PORT}`);
    }
})


// middleware
// middleware is a function that has access to http request object
// http response object and next middleware function
// there are five types of middleware
// 1 application level middleware
// 2 routing level middleware
// 3 error handling middleware
// 4 inbuilt middleware
// 5 third party middleware

// app.use() // app.use is configuration block for middleware

// the order of middlware matters